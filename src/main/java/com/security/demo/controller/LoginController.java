package com.security.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.security.demo.Service.TokenService;
import com.security.demo.entity.UserEntity;

@RestController
@RequestMapping("/api")
public class LoginController {
    
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private UserDetailsService myUserDetailsService;

    @PostMapping("/login")
    public String login(@RequestBody UserEntity userEntity) {
        Authentication token = new UsernamePasswordAuthenticationToken(userEntity.getUsername(), userEntity.getPassword());
        authenticationManager.authenticate(token);
        UserDetails userDetails = myUserDetailsService.loadUserByUsername(userEntity.getUsername());
        return tokenService.generateToken(userDetails);
    }

    @GetMapping("/hello")
    public String hello() {
        return "hello";
    }

}
