package com.security.demo.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import com.security.demo.entity.UserEntity;

public interface UserMapper {

    @Select("select * from user where username = #{username}")
    public UserEntity selectByUsername(String username);

    @Insert("insert into user (username, password) values (#{username}, #{encodePass})")
    public void insert(String username, String encodePass);

}
